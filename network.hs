import System.IO
import System.Environment
import Data.Char
import Data.List

pair' :: a -> [a] -> [(a, a)]
pair' x [] = []
pair' x xs = [(x, y) | y<-xs] ++ pair xs

pair :: [a] -> [(a,a)]
pair xs = pair' (head xs) (tail xs)

main = do
   args <- getArgs
   content <- readFile (args !! 0)
   let len = read (args !! 1) :: Int

   let linesOfFiles = lines content
   let words = filter (all isLower) (filter ((len==).length) linesOfFiles)

   let match (x,y) = x==y
   let diff (a, b) = length (filter (not.match) (zip a b))

   let pairs = filter ((==1).diff) (pair words) 

   let wordset = nub.concat $ (map  (\(x,y) -> [x,y]) pairs)

   let tupleStr (a,b) = "[\"" ++ a ++ "\",\"" ++ b ++ "\"]"
   let quote a = "\""++ a ++ "\""
   let arrayStr a = "["++(intercalate "," a)++"]"

   let wordStr = arrayStr (map quote wordset)
   let pairStr = arrayStr (map tupleStr pairs)

   putStrLn (arrayStr [wordStr, pairStr])
