window.onload = function() {
  var svg = d3.select("svg"),
    width = +svg.attr("width"),
    height = +svg.attr("height");

  var zg = svg.append("g");
  var zoom = d3.zoom()
    .scaleExtent([0.05, 40])
    .translateExtent([[-width*5, -height*5], [width *5, height *5]])
    .on("zoom", zoomed);
  svg.call(zoom);
  function zoomed() {
    zg.attr("transform", d3.event.transform);
  }

  var simulation = d3.forceSimulation()
    .force("link", d3.forceLink().id(function(d) { return d.id; }))
    .force("charge", d3.forceManyBody())
    .force("center", d3.forceCenter(width / 2, height / 2));

  var color = d3.scaleOrdinal(d3.schemeCategory20);

  d3.json("five_tiny.json", function(error, graph) {
    if (error) throw error;
    var nodes = graph[0].map(d=>({"id": d}));
    var edges = graph[1].map(d=>({"source":d[0], "target":d[1]}));
    window.console.log(nodes);
    window.console.log(edges);
    var link = zg.append("g")
        .attr("class", "links")
      .selectAll("line")
      .data(edges)
      .enter().append("line")
        .attr("stroke-width", 2 );

    var node = zg.append("g")
      .attr("class", "nodes")
      .selectAll("text")
      .data(nodes)
      .enter().append("text")
        .text(d => d.id)
        .attr("fill", d=>color(d.id[0]))
        .call(d3.drag()
          .on("start", dragstarted)
          .on("drag", dragged)
          .on("end", dragended));
         

    simulation
      .nodes(nodes)
      .on("tick", ticked);

    simulation.force("link")
      .links(edges);

    function ticked() {
      link
        .attr("x1", function(d) { return d.source.x; })
        .attr("y1", function(d) { return d.source.y; })
        .attr("x2", function(d) { return d.target.x; })
        .attr("y2", function(d) { return d.target.y; });

      node
        .attr("transform", function(d) { return "translate(" + d.x + "," + d.y + ")"; })
      }

      function dragstarted(d) {
        if (!d3.event.active) simulation.alphaTarget(0.3).restart();
        d.fx = d.x;
        d.fy = d.y;
      }

    function dragged(d) {
      d.fx = d3.event.x;
      d.fy = d3.event.y;
    }

    function dragended(d) {
      if (!d3.event.active) simulation.alphaTarget(0);
      d.fx = null;
      d.fy = null;
    };
  });
}
  
